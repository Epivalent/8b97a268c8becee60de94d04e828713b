Tim Cook was born on November 1, 1960. To calculate his age on June 10, 2024:

1. From November 1, 1960 to November 1, 2023 is 63 years.
2. From November 1, 2023 to June 10, 2024 is an additional period of a little over seven months.

So, on June 10, 2024, Tim Cook will be **63 years old** and will turn **64** later that year in November.

# Mon 10 Jun 20:44:21 CEST 2024 - how old is Tim Cook on 10 Jun 2024?